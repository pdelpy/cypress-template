const cucumber = require("cypress-cucumber-preprocessor").default;
const browserify = require("@cypress/browserify-preprocessor");
const allureWriter = require("@shelex/cypress-allure-plugin/writer");

module.exports = (on, config) => {
  const options = browserify.defaultOptions;
  (options.typescript = require.resolve("typescript")),
    options.browserifyOptions.extensions.unshift(".ts");
  options.browserifyOptions.plugin.unshift([
    "tsify",
    { project: "tsconfig.json" },
  ]);
  on("file:preprocessor", cucumber(options));
  allureWriter(on, config);
  return config;
};
