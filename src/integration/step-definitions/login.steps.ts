import { Given, Then, When } from "cypress-cucumber-preprocessor/steps";

import LoginPage from "../pages/login.page";
import SecurePage from "../pages/secure.page";

const pages = {
  login: LoginPage,
};

Given(/^I am on the (\w+) page$/, async (page) => {
  await pages[page].open();
});

When(/^I login with (\w+) and (.+)$/, async (username, password) => {
  await LoginPage.login(username, password);
});

Then(/^I should see a flash message saying (.*)$/, async (message) => {
  SecurePage.flashAlert.should("be.visible");
  SecurePage.flashAlert.should("contain", message);
});
