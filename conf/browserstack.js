var fs = require('fs');

const config = {
  "auth": {
    "username": process.env.BROWSERSTACK_USER,
    "access_key": process.env.BROWSERSTACK_KEY,
  },
  "browsers": [{
      "browser": "chrome",
      "os": "Windows 10",
      "versions": ["latest"]
    },
  ],
  "run_settings": {
    "cypress_config_file": "./cypress.json",
    "cypress_version": "6",
    "project_name": 'Cypress',
    "parallels": "1",
    "env": {
      "TAGS": process.env.TAGS
    },
    "npm_dependencies": {
      "cypress": "^6.0.1",
      "@types/node": "^14.14.31",
      "cypress-cucumber-preprocessor": "^4.0.1",
      "cypress-image-snapshot": "^4.0.0",
      "typescript": "^4.1.2",
      "tsify": "^5.0.2",
      "@shelex/cypress-allure-plugin": "^2.3.6"
    },
    "package_config_options": {
      "cypress-cucumber-preprocessor": {
        "nonGlobalStepDefinitions": false,
        "stepDefinitions": "src/integration/step-definitions",
        "cucumberJson": {
          "generate": true,
          "outputFolder": ".tmp/cucumber-results",
          "filePrefix": "",
          "fileSuffix": ".cucumber"
        }
      }
    }
  }
};

fs.writeFile('browserstack.json', JSON.stringify(config), function (err) {
  if (err) throw err;
  console.log('browserstakc is created!');
});
